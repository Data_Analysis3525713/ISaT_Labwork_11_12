import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import datasets
from sklearn.preprocessing import MinMaxScaler
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
import umap
from sklearn.cluster import KMeans, DBSCAN
from sklearn.metrics import silhouette_score 
from fcmeans import FCM
from math import sqrt

def calculate_kn_distance (X, k):
    kn_distance = []
    for i in range(len(X)):
        eucl_disct = []
        for j in range(len(X)):
            eucl_disct.append(sqrt(
                (X[i,0] - X[j,0]) ** 2 +
                (X[i,1] - X[j,1]) ** 2
            ))

        eucl_disct.sort()
        kn_distance.append(eucl_disct[k])

    return kn_distance

def prepare_data (path):
    # reading a data file
    dataset = pd.read_csv(path, sep=",")
    # preparing data
    dataframe = pd.DataFrame(dataset)
    scaler = MinMaxScaler().fit(dataframe.to_numpy())
    scaled_data = scaler.transform(dataframe.to_numpy())
    # making matrix of distances 
    distance_matrix = linkage(scaled_data, method="average", metric="euclidean")

    return distance_matrix, dataframe, scaled_data



distance_matrix, dataframe, scaled_data = prepare_data("./hayes+roth/hayes-roth.data")
# ---------------------- hierarchic algorithm ----------------------
dataframe['hi_cluster_labels'] = fcluster(distance_matrix, 3, criterion="maxclust")
# making visualization
fig, axes = plt.subplots(1,1, figsize = (16,8))
axes.scatter(scaled_data[:,0], scaled_data[:,1], c=dataframe['hi_cluster_labels'], cmap=plt.cm.Set1)
axes.set_title('Метки кластеров, найденные иерархическим алгоритмом', fontsize=24)
plt.show()
fig, axes = plt.subplots(1,1, figsize = (16,8))
axes.scatter(scaled_data[:,3], scaled_data[:,4], c=dataframe['hi_cluster_labels'], cmap=plt.cm.Set1)
axes.set_title('Метки кластеров, найденные иерархическим алгоритмом', fontsize=24)
plt.show()


## iterational algorithms
# ---------------------- KMeans algorithm ----------------------
WCSS = []
Silh = []
for i in range(2,11):
    kmeans = KMeans(n_clusters=i, init="k-means++", max_iter=300, n_init=10, random_state=0)
    kmeans.fit(scaled_data)
    dataframe['km_cluster_labels'] = kmeans.fit_predict(scaled_data)
    WCSS.append(kmeans.inertia_)
    Silh.append(silhouette_score(scaled_data, dataframe["km_cluster_labels"], metric="euclidean"))
print(f"Силуэт для KMeans:\n{Silh}")
# Визуализация силуэта и плеча для KMeans
fig = plt.figure(figsize=(5,5))
plt.plot(range(2,11), WCSS)
plt.title('Индекс кластерного плеча для KMeans')
plt.xlabel('Число кластеров')
plt.ylabel('Elbow score')
plt.show()
plt.plot(range(2,11), Silh)
plt.title('Индекс кластерного силуэта для KMeans')
plt.xlabel('Число кластеров')
plt.ylabel('Silhouette score')
plt.show()
# counting KMeans when 3 clusters
kmeans = KMeans(n_clusters=3, init="k-means++", max_iter=300, n_init=10, random_state=0)
kmeans.fit(scaled_data)
dataframe['km_cluster_labels'] = kmeans.fit_predict(scaled_data)
# visualization when 3 clusters made by KMeans
fig, axes = plt.subplots(1,1, figsize = (16,8))
axes.scatter(scaled_data[:,0], scaled_data[:,1], c=dataframe['km_cluster_labels'], cmap=plt.cm.Set1)
axes.set_title('Метки кластеров, найденные алгоритмом KMeans', fontsize=24)
plt.show()


# ---------------------- CMeans algorithm ----------------------
Silh = []
for i in range(2, 11):
    fcm = FCM(n_clusters=i, m=2, max_iter=300, random_state=0)
    fcm.fit(scaled_data)
    dataframe['fcm_cluster_labels'] = fcm.predict(scaled_data)
    Silh.append(silhouette_score(scaled_data, dataframe['fcm_cluster_labels'], metric='euclidean'))
print(f"Силуэт для fcm:\n{Silh}")
# visualization of silhouette
plt.plot(range(2,11), Silh)
plt.title('Индекс кластерного силуэта для FCMeans')
plt.xlabel('Число кластеров')
plt.ylabel('Silhouette score')
plt.show()
# deviding into 3 clusters
fcm = FCM(n_clusters=3, m=2, max_iter=300, random_state=0)
fcm.fit(scaled_data)
dataframe['fcm_cluster_labels'] = fcm.predict(scaled_data)
# visualization for CMeans 3 Clusters
fig, axes = plt.subplots(1,1, figsize = (16,8))
axes.scatter(scaled_data[:,0], scaled_data[:,1], c=dataframe['fcm_cluster_labels'], cmap=plt.cm.Set1)
axes.set_title('Метки кластеров, найденные алгоритмом FCMeans', fontsize=24)
plt.show()


# ---------------------- DBSCAN algorithm ----------------------
eps_dist = calculate_kn_distance(scaled_data, 6)
# Гистограмма расстояний для выбора подходящего Eps
plt.hist(eps_dist, bins=30)
plt.ylabel('Число объектов', size=14)
plt.xlabel('Eps-расстояние', size=14)
plt.show()
db = DBSCAN(eps=0.6, min_samples=3).fit(scaled_data)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
# число кластеров в метках без учета шума, если он имеется
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list (labels).count (-1)

#labels_true = dataframe['target']
print("Число кластеров: %d" % n_clusters_)
print("Число шумовых объектов: %d" % n_noise_)
# Дополнительная информация, которая не сработала, потому что нет 
""" print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f" % metrics.adjusted_rand_score(labels_true, labels))
print (
"Adjusted Mutual Information: %0.3f"
    % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette score: %0.3f" % metrics.silhouette_score(scaled_data, labels)) """
# visualization
plt.figure(figsize=(8,8))
unique_labels = set(labels)
colors = [plt.cm.Spectral(each) for each in np.linspace(0,1,len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        col = [0,0,0,1]
    class_member_mask = labels == k
    xy = scaled_data[class_member_mask & core_samples_mask]
    plt.plot(
        xy[:,0],
        xy[:,1],
        "o",
        markerfacecolor = tuple(col),
        markeredgecolor = "k",
        markersize = 14
    )
    xy = scaled_data[class_member_mask & ~core_samples_mask]
    plt.plot(
        xy[:,0],
        xy[:,1],
        "o",
        markerfacecolor = tuple(col),
        markeredgecolor = "k",
        markersize = 6
    )
    plt.title("Число кластеров: %d" % n_clusters_, size = 14)
    plt.show()




















# working with UMAP
""" reducer = umap.UMAP()
plt.scatter(
    scaled_data[:, 0],
    scaled_data[:, 1],
    c=[sns.color_palette()[x] for x in dataframe.map({"Adelie":0, "Chinstrap":1, "Gentoo":2})])
plt.gca().set_aspect('equal', 'datalim')
plt.title('UMAP projection of the Penguin dataset', fontsize=24); """


## making picture
# setting canvas
""" fig = plt.figure(figsize=(15,30))
fig.patch.set_facecolor('white') """
# making visualization
""" fig, axes = plt.subplots(1,1, figsize = (16,8))
axes.scatter(scaled_data[:,3], scaled_data[:,4], c=dataframe['cluster_labels'], cmap=plt.cm.Set1)
axes.set_title('Метки кластеров, найденные алгоритмом', fontsize=16)
plt.show() """

